# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.backend import TableHandler


class Address(ModelSQL, ModelView):
    _name = 'party.address'

    preferred = fields.Boolean('Preferred')
    home = fields.Boolean('Home')

    def init(self, module_name):
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # Migration from 2.0: Field 'default' renamed to 'preferred'
        table.column_rename('default', 'preferred')

        super(Address, self).init(module_name)

Address()

